package net.restaurant.scraper.spring.service;

import java.util.List;

import net.restaurant.scraper.spring.dao.RestaurantDao;
import net.restaurant.scraper.spring.model.Address;
import net.restaurant.scraper.spring.model.Area;
import net.restaurant.scraper.spring.model.PopularCity;
import net.restaurant.scraper.spring.model.Restaurant;
import net.restaurant.scraper.spring.model.RestaurantFoodType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestaurantServiceImpl implements RestaurantService {
	@Autowired
	private RestaurantDao restaurantDao;

	@Override
	public List<PopularCity> getPopularCityList() {
		return restaurantDao.getPopularCityList();
	}

	@Override
	public PopularCity getPupularCity() {
		return restaurantDao.getPupularCity();
	}

	@Override
	public List<Area> getArea(Integer cityId) {
		return restaurantDao.getArea(cityId);
	}

	@Override
	public List<Address> getAddressList(String areaName) {
		List<Address> addressList = restaurantDao.getAddressList(areaName);
		String[] images = { "image1.jpg", "image2.jpg", "image3.jpg",
				"image4.jpg", "image5.jpg", "image6.jpg", "image8.jpg",
				"image9.jpg", "image10.jpg", "image11.jpg", "image12.jpg",
				"image13.jpg", "image14.jpg", "image15.jpg" };
		int count = 0;
		for (Address address : addressList) {
			Restaurant res = restaurantDao.getRestaurant(address);
			res.setImageName(images[count]);
			address.setRestaurant(res);
			count++;
		}
		return addressList;
	}

	@Override
	public List<String> getRestaurantType() {
		return restaurantDao.getRestaurantType();
	}

	@Override
	public List<Address> getAddressList() {
		String[] images = { "image1.jpg", "image2.jpg", "image3.jpg",
				
				"image4.jpg", "image5.jpg", "image6.jpg", "image8.jpg",
				"image9.jpg", "image10.jpg", "image11.jpg", "image12.jpg",
				"image13.jpg", "image14.jpg", "image15.jpg" };
		List<Address> list = restaurantDao.getAddressList();
		int count = 0;
		for (Address address : list) {
			Restaurant res = restaurantDao.getRestaurant(address);
			res.setImageName(images[count]);
			address.setRestaurant(res);
			count++;
		}
		return list;
	}

	@Override
	public List<Address> getRestaurantAddressDetails(Integer restaurantId) {
		return restaurantDao.getRestaurantAddressDetails(restaurantId);
	}

	@Override
	public Restaurant getRestaurantDetail(Integer restaurantId) {
		return restaurantDao.getRestaurantDetail(restaurantId);
	}

	@Override
	public List<RestaurantFoodType> getRestaurantFoodType(Integer restaurantId) {
		return restaurantDao.getRestaurantFoodType(restaurantId);
	}
}
