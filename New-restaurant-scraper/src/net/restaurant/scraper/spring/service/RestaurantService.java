package net.restaurant.scraper.spring.service;
 
import java.util.List;

import net.restaurant.scraper.spring.model.Address;
import net.restaurant.scraper.spring.model.Area;
import net.restaurant.scraper.spring.model.PopularCity;
import net.restaurant.scraper.spring.model.Restaurant;
import net.restaurant.scraper.spring.model.RestaurantFoodType;
 
public interface RestaurantService {
	public Restaurant getRestaurantDetail(Integer restaurantId);
	public List<PopularCity> getPopularCityList();
	public PopularCity getPupularCity();
	public List<Area> getArea(Integer cityId);
	public List<Address> getAddressList(String areaName);
	public List<String> getRestaurantType();
	public List<Address> getAddressList();
	public List<Address> getRestaurantAddressDetails(Integer restaurantId);
	public List<RestaurantFoodType> getRestaurantFoodType(Integer restaurantId);
} 