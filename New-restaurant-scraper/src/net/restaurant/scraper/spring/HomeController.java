package net.restaurant.scraper.spring;

import java.util.List;

import net.restaurant.scraper.spring.model.Area;
import net.restaurant.scraper.spring.model.PopularCity;
import net.restaurant.scraper.spring.model.SearchRestaurant;
import net.restaurant.scraper.spring.service.RestaurantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/home")
public class HomeController {

	@Autowired
	private RestaurantService restaurantService;

	@RequestMapping(value = "/home")
	public ModelAndView home() {
		ModelAndView model = new ModelAndView("index");
		PopularCity pCity = restaurantService.getPupularCity();
		List<Area> aList = restaurantService.getArea(pCity.getId());
		model.addObject("popularCity", pCity);
		model.addObject("area", aList);
		model.addObject("popularCityCount",restaurantService.getPopularCityList().size());
		model.addObject("viewRestaurants",new SearchRestaurant());
		return model;
	}

	@RequestMapping(value = "/aboutus")
	public ModelAndView aboutus() {
		ModelAndView model = new ModelAndView("aboutus");
		return model;
	}

	@RequestMapping(value = "/contactus")
	public ModelAndView contactus() {
		ModelAndView model = new ModelAndView("contactus");
		return model;
	}
}