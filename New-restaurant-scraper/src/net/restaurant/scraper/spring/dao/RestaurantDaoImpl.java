package net.restaurant.scraper.spring.dao;

import java.util.List;

import net.restaurant.scraper.spring.model.Address;
import net.restaurant.scraper.spring.model.Area;
import net.restaurant.scraper.spring.model.PopularCity;
import net.restaurant.scraper.spring.model.Restaurant;
import net.restaurant.scraper.spring.model.RestaurantFoodType;
import net.restaurant.scraper.spring.model.RestaurantType;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class RestaurantDaoImpl implements RestaurantDao {
	@Autowired
	private SessionFactory sessionFactory;
	private Session session;

	@Override
	@Transactional
	public List<Restaurant> getRestaurantList(String  cityName) {
		session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Restaurant.class);
		criteria.add(Restrictions.like("area", cityName));
		List<Restaurant> list = session.createCriteria(Restaurant.class).list();
		session.close();
		return list;
	}

	@Override
	public Address getAddress(String  areaName) {
		session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Address.class);
		//criteria.add(Restrictions.like("area", areaName));
		session.close();
		return null;
	}

	@Override
	public List<PopularCity> getPopularCityList() {
		session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<PopularCity> list = session.createCriteria(PopularCity.class).list();
		session.close();
		return list;
	}

	@Override
	public PopularCity getPupularCity() {
		session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		PopularCity city = (PopularCity) session.get(PopularCity.class,6);
		session.close();
		return city;
	}

	@Override
	public List<Area> getArea(Integer cityId) {
		session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Area.class);
		criteria.add(Restrictions.eq("city_id", cityId));
		List<Area> list = null;
		if(criteria.list() != null)
			list = criteria.list();
		session.close();
		return list;
	}

	@Override
	public List<Address> getAddressList(String areaName) {
		session = sessionFactory.openSession();
		String areaNameS = areaName.split(",")[0];
		Criteria criteria = session.createCriteria(Address.class);
		criteria.add(Restrictions.like("res_address", "%"+areaNameS+"%", MatchMode.END));
		List<Address> list = criteria.list();
		session.close();
		return list;
	}

	@Override
	public Restaurant getRestaurant(Address address) {
		session = sessionFactory.openSession();
		Restaurant res = (Restaurant) session.get(Restaurant.class,address.getRestaurant_id());
		session.close();
		return res;
	}

	@Override
	public List<String> getRestaurantType() {
		session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<String> list = session.createCriteria(RestaurantType.class).setProjection(Projections.distinct(Projections.property("rtype"))).list();
		session.close();
		return list;
	}

	@Override
	public List<Address> getAddressList() {
		session = sessionFactory.openSession();
		List<Address> list  = session.createCriteria(Address.class).list();
		session.close();
		return list;
	}

	@Override
	public List<Address> getRestaurantAddressDetails(Integer restaurantId) {
		session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Address.class);
		List<Address> addressList = criteria.add(Restrictions.eq("restaurant_id", restaurantId)).list();
		session.close();
		return addressList;
	}

	@Override
	public Restaurant getRestaurantDetail(Integer restaurantId) {
		session = sessionFactory.openSession();
		Restaurant res = (Restaurant) session.get(Restaurant.class,restaurantId);
		session.close();
		return res;
	}

	@Override
	public List<RestaurantFoodType> getRestaurantFoodType(Integer restaurantId) {
		session = sessionFactory.openSession();
		System.out.println("restaurant id = " + restaurantId);
		//List<RestaurantFoodType> restaurantFoodList = session.createCriteria(RestaurantFoodType.class).add(Restrictions.eq("restaurantid", restaurantId)).list();
		List<RestaurantFoodType> restaurantFoodList = session.createQuery("from RestaurantFoodType e where restaurantid = " + restaurantId).list();
		session.close();
		System.out.println(restaurantFoodList);
		return restaurantFoodList;
	}

	

}