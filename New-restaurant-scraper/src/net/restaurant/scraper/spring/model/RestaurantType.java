package net.restaurant.scraper.spring.model;

public class RestaurantType {
	private Integer id;
	private String rtype;
	private String description;
	private Integer restaurantId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRtype() {
		return rtype;
	}

	public void setRtype(String rtype) {
		this.rtype = rtype;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	@Override
	public String toString() {
		return "RestaurantType [id=" + id + ", rtype=" + rtype
				+ ", description=" + description + ", restaurantId="
				+ restaurantId + "]";
	}

}
