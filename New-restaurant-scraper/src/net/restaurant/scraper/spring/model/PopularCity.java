package net.restaurant.scraper.spring.model;


public class PopularCity {
	private Integer id;
	private String city_name;
	private String city_code;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public String getCity_code() {
		return city_code;
	}

	public void setCity_code(String city_code) {
		this.city_code = city_code;
	}

	@Override
	public String toString() {
		return "PopularCity [id=" + id + ", city_name=" + city_name
				+ ", city_code=" + city_code;
	}

}
