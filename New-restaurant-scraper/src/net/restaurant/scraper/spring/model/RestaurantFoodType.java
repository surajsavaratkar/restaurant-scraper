package net.restaurant.scraper.spring.model;

public class RestaurantFoodType {
	private Integer id;
	private String foodtype;
	private String description;
	private Integer restaurantid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFoodtype() {
		return foodtype;
	}

	public void setFoodtype(String foodtype) {
		this.foodtype = foodtype;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getRestaurantid() {
		return restaurantid;
	}

	public void setRestaurantid(Integer restaurantid) {
		this.restaurantid = restaurantid;
	}

	@Override
	public String toString() {
		return "RestaurantFoodType [id=" + id + ", foodtype=" + foodtype
				+ ", description=" + description + ", restaurantid="
				+ restaurantid + "]";
	}

}
