package net.restaurant.scraper.spring.model;

public class SearchRestaurant {
	private String cityName;
	private String areaName;
	private Integer cityId;
	private Integer areaId;

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	@Override
	public String toString() {
		return "SearchRestaurant [cityName=" + cityName + ", areaName="
				+ areaName + ", cityId=" + cityId + ", areaId=" + areaId + "]";
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

}
