package net.restaurant.scraper.spring.model;

public class Restaurant {
	private int id;
	private String name;
	private String description;
	private String address_id;
	private String imageName;
	private RestaurantType restaurantType;

	public RestaurantType getRestaurantType() {
		return restaurantType;
	}

	public void setRestaurantType(RestaurantType restaurantType) {
		this.restaurantType = restaurantType;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress_id() {
		return address_id;
	}

	public void setAddress_id(String address_id) {
		this.address_id = address_id;
	}

	@Override
	public String toString() {
		return "Restaurant [id=" + id + ", name=" + name + ", description="
				+ description + ", address_id=" + address_id + ", imageName="
				+ imageName + ", restaurantType=" + restaurantType + "]";
	}

}