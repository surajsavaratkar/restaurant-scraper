package net.restaurant.scraper.spring.model;

public class Address {
	private Integer id;
	private String res_address;
	private String state;
	private String country;
	private Integer area_id;
	private Integer restaurant_id;
	private Restaurant restaurant;

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public Integer getRestaurant_id() {
		return restaurant_id;
	}

	public void setRestaurant_id(Integer restaurant_id) {
		this.restaurant_id = restaurant_id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRes_address() {
		return res_address;
	}

	public void setRes_address(String res_address) {
		this.res_address = res_address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getArea_id() {
		return area_id;
	}

	public void setArea_id(Integer area_id) {
		this.area_id = area_id;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", res_address=" + res_address
				+ ", state=" + state + ", country=" + country + ", area_id="
				+ area_id + ", restaurant_id=" + restaurant_id
				+ ", restaurant=" + restaurant + "]";
	}

}
