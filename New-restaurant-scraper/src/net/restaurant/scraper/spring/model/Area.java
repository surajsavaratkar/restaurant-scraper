package net.restaurant.scraper.spring.model;

public class Area {
	private Integer id;
	private String area_name;
	private String area_pin;
	private String land_mark;
	private Integer city_id;

	@Override
	public String toString() {
		return "Area [id=" + id + ", area_name=" + area_name + ", area_pin="
				+ area_pin + ", land_mark=" + land_mark + ", city_id="
				+ city_id + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getArea_name() {
		return area_name;
	}

	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}

	public String getArea_pin() {
		return area_pin;
	}

	public void setArea_pin(String area_pin) {
		this.area_pin = area_pin;
	}

	public String getLand_mark() {
		return land_mark;
	}

	public void setLand_mark(String land_mark) {
		this.land_mark = land_mark;
	}

	public Integer getCity_id() {
		return city_id;
	}

	public void setCity_id(Integer city_id) {
		this.city_id = city_id;
	}

}
