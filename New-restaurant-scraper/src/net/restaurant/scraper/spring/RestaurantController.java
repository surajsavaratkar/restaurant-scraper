package net.restaurant.scraper.spring;

import net.restaurant.scraper.spring.model.SearchRestaurant;
import net.restaurant.scraper.spring.service.RestaurantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/search")
public class RestaurantController {
     
	@Autowired
	private RestaurantService restaurantService;
     
    @RequestMapping(value="/restaurant",method = {RequestMethod.GET})
    public ModelAndView home(@ModelAttribute("viewRestaurants") SearchRestaurant searchRestaurant) {
    	ModelAndView model = new ModelAndView("searchrestaurant");
    	model.addObject("addressListnrestList",restaurantService.getAddressList(searchRestaurant.getAreaName()));
    	model.addObject("restaurantType",restaurantService.getRestaurantType());
    	model.addObject("popularCityList",restaurantService.getPopularCityList());
        return model;
    }
    @RequestMapping(value="/allRestaurantList",method = {RequestMethod.GET})
    public ModelAndView allRestaurantList() {
    	ModelAndView model = new ModelAndView("allrestaurantlist");
    	model.addObject("popularCityList",restaurantService.getPopularCityList());
    	model.addObject("restaurantType",restaurantService.getRestaurantType());
    	model.addObject("alladdressListnrestList",restaurantService.getAddressList());
    	return model;
    }
    @RequestMapping(value="/searchRestaurantDetails",method = {RequestMethod.GET})
    public ModelAndView searchRestaurantDetails(@ModelAttribute("restaurantId") Integer restaurantId) {
    	ModelAndView model = new ModelAndView("retaurantdetail");
    	model.addObject("popularCityList",restaurantService.getPopularCityList());
    	model.addObject("restaurantType",restaurantService.getRestaurantType());
    	model.addObject("restaurantAddessDetail",restaurantService.getAddressList());
    	model.addObject("restaurantDetail",restaurantService.getRestaurantDetail(restaurantId));
    	model.addObject("restaurantFoodType",restaurantService.getRestaurantFoodType(restaurantId));
    	return model;
    }
     
}