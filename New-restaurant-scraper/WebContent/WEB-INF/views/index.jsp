<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<html>
<head>
<title>Home</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="../custom-css/home.css" rel="stylesheet" type="text/css">
<link href="../custom-css/div-formatter.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="../custom-js/css/font-awesome.min.css" />
<link rel="stylesheet" href="../custom-js/css/style.css" />
<script src="../custom-js/modernizr-2.6.2.min.js"></script>




</head>
<body style="padding-top: 8px; background-color: #E6DACE;">



	<div class="container-fluid" style="padding: 5">

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="<c:url value='../home/home'/>">Restaurant
						Scraper</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="<c:url value='../home/home'/>">Home</a></li>
					<li><a href="<c:url value='../home/aboutus'/>">About Us</a></li>
					<li><a href="<c:url value='../home/contactus'/>">Contact
							Us</a></li>
				</ul>
			</div>
		</nav>
		<div class="row div-content-center">
			<div class="row-fluid">
				<div class="col-lg-3 col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-comments fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">26</div>
									<div>New Restaurant</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="panel-footer">
								<span class="pull-left">View Details</span> <span
									class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="panel panel-green">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-tasks fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">12</div>
									<div>View Order</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="panel-footer">
								<span class="pull-left">View Cart</span> <span
									class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="panel panel-yellow">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-shopping-cart fa-5x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge">124</div>
									<div>New Orders!</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="panel-footer">
								<span class="pull-left">View Details</span> <span
									class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="panel panel-red">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-3">
									<i class="fa fa-support fa-4x"></i>
								</div>
								<div class="col-xs-9 text-right">
									<div class="huge"><c:out value="${popularCityCount}"/> </div>
									<div>Popular Cities</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="panel-footer">
								<span class="pull-left">View Details</span> <span
									class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>




	</div>
	<!-- -------------------------------------------------------------------- -->

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2 col-md-8 col-md-offset-2"
				style="background-color: rgb(14, 240, 142);">
				<font size="10" face="" class="text-center login-parent-title">Order
					delicious food online!</font>
				<h1 class="text-center login-title">Select your city and area
					to continue..</h1>
				<div class="row-fluid" style="padding-bottom: 10px;">
					<div class="container-fluid div-content-center">
						<div class="col-sm-2 col-md-3"></div>
						<html:form action="../search/restaurant" method="get" commandName="viewRestaurants">
							<div class="col-sm-2 col-md-3">
								<div class="form-group">
									<select class="form-control" id="sel1" name="cityName">
										<option>select City</option>
										<option value="<c:out value="${popularCity.id}"/>,<c:out value="${popularCity.city_name}"/>"><c:out
												value="${popularCity.city_name}" />
										</option>
									</select>
								</div>
							</div>
							<div class="col-sm-2 col-md-3">
								<div class="form-group">
									<select class="form-control" id="sel1" name="areaName">
										<option>select Area</option>
										<c:forEach items="${area}" var="areaL">
											<option value="<c:out value="${areaL.area_name}"/>,<c:out value="${areaL.id}"/>"><c:out
													value="${areaL.area_name}" /></option>
										</c:forEach>
									</select>
								</div>

								<div class="btn-group"></div>
							</div>
							<div class="div-content-center">
								<input type="submit" class="btn btn-primary" value="Search">
							</div>
						</html:form>
						<div class="col-sm-2 col-md-3"></div>
					</div>
				</div>
				<!-- <a href="#" class="text-center new-account">Create an account </a> -->
				<div class="div-content-center" style="padding-bottom: 10px;">
					<button class="btn btn-primary " data-toggle="modal"
						data-target="#login-modal">Please sign In</button>
				</div>
			</div>
		</div>
	</div>

	<!--  login pop up -->
	<!-- <div class="container-fluid">
		<div class="row">
			<div class="col-md-offset-4 col-lg-offset-4 col-md-4 col-lg-4">
				<h1 class="text-center">Twitter Bootstrap Modal Login Popup</h1>
				<button id='modal-launcher' class="btn btn-primary btn-lg"
					data-toggle="modal" data-target="#login-modal">Please sign
					In - Modal</button>
			</div>
		</div>
	</div> -->



</body>
</html>



<div class="modal fade" id="login-modal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header login_modal_header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h2 class="modal-title" id="myModalLabel">Login to Your Account</h2>
			</div>
			<div class="modal-body login-modal">
				<p>Enter User name and password to login</p>
				<br />
				<div class="clearfix"></div>
				<div id='social-icons-conatainer'>
					<div class='modal-body-left'>
						<div class="form-group">
							<input type="text" id="username" placeholder="Enter your name"
								value="" class="form-control login-field"> <i
								class="fa fa-user login-field-icon"></i>
						</div>

						<div class="form-group">
							<input type="password" id="login-pass" placeholder="Password"
								value="" class="form-control login-field"> <i
								class="fa fa-lock login-field-icon"></i>
						</div>

						<a href="#" class="btn btn-success modal-login-btn">Login</a> <a
							href="#" class="login-link text-center">Lost your password?</a>
					</div>

					<div class='modal-body-right'>
						<div class="modal-social-icons">
							<a href='#' class="btn btn-default facebook"> <i
								class="fa fa-facebook modal-icons"></i> Sign In with Facebook
							</a> <a href='#' class="btn btn-default twitter"> <i
								class="fa fa-twitter modal-icons"></i> Sign In with Twitter
							</a> <a href='#' class="btn btn-default google"> <i
								class="fa fa-google-plus modal-icons"></i> Sign In with Google
							</a> <a href='#' class="btn btn-default linkedin"> <i
								class="fa fa-linkedin modal-icons"></i> Sign In with Linkedin
							</a>
						</div>
					</div>
					<div id='center-line'>OR</div>
				</div>
				<div class="clearfix"></div>

				<div class="form-group modal-register-btn">
					<button class="btn btn-default">New User Please Register</button>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer login_modal_footer"></div>
		</div>
	</div>
</div>