<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="f"%>
<html>
<head>
<title>Restaurant List</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="../custom-css/home.css" rel="stylesheet" type="text/css">
<link href="../custom-css/build.css" rel="stylesheet" type="text/css">
</head>
<body style="padding-top: 8px; background-color: #E6DACE;">



	<div class="container-fluid" style="padding: 5">

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Restaurant Scraper</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="<c:url value='home/home'/>">Home
					</a></li>
					<li><a href="#">About Us</a></li>
					<li><a href="#">Contact Us</a></li>
				</ul>
			</div>
		</nav>

		<ul class="breadcrumb">
			<li><a href="<c:url value='../home/home'/>">Home </a></li>
			<li><a href="<c:url value='allRestaurantList'/>">
					Restaurants</a></li>
			<li><a href="<c:url value=''/>"> <c:out
						value="${restaurantDetail.name }" />
			</a></li>
			<li class="active" id="categoryBreadCrumb" style="display: none;"></li>
		</ul>


		<div class="row ">
			<div class="col-lg-3 col-md-3 col-sm-12">
				<img alt="restaurant image" src="../images/image10.jpg">
			</div>
			<div class="col-lg-9 col-md-9 col-sm-12">
				<h2>
					<c:out value="${restaurantDetail.name }" />
				</h2>
				<ul class="breadcrumb" style="background-color: #E6DACE;">
					<c:forEach items="${restaurantFoodType}" var="rfoodType">
						<li class="active" id="categoryBreadCrumb"><c:out
								value="${rfoodType.foodtype}"></c:out></li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>





</body>
</html>