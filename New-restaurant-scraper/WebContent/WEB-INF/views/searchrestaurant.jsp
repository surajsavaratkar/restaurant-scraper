<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="f"%>
<html>
<head>
<title>Restaurant List</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="../custom-css/home.css" rel="stylesheet" type="text/css">
<link href="../custom-css/build.css" rel="stylesheet" type="text/css">
</head>
<body style="padding-top: 8px; background-color: #E6DACE;">
	<div class="container-fluid" style="padding: 5">

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Restaurant Scraper</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="<c:url value='home/home'/>">Home
					</a></li>
					<li><a href="#">About Us</a></li>
					<li><a href="#">Contact Us</a></li>
				</ul>
			</div>
		</nav>

		<ul class="breadcrumb">
			<li><a href="<c:url value='../home/home'/>">Home </a></li>
			<li><a href="<c:url value='allRestaurantList'/>">
					Restaurants</a></li>
			<li class="active" id="categoryBreadCrumb" style="display: none;"></li>
		</ul>


		<div class="row" id="products">

			<div class="col-lg-3 col-md-3 col-sm-12">
				<div class="panel-group" id="accordionNo">
					<div class="panel panel-default groupPanel">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" href="#collapseRestaurant"
									class="collapseWill active "> <span class="pull-left">
										<i class="fa fa-caret-right"></i>
								</span> Filter Restaurants
								</a>
							</h4>
						</div>
						<div id="collapseRestaurant" class="panel-collapse collapse in">
							<div class="panel-body ">
								<ul class="nav nav-pills nav-stacked tree" id="groupFilter">
									<c:forEach items="${restaurantType}" var="rType">
										<li class="" id="group-35"><a href="#"
											class="dropdown-tree-a"> <!-- <span
												class="badge pull-right">0</span> --> <c:out
													value="${rType}" />
										</a></li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" href="#collapseCityNames"
									class="collapseWill active "> <span class="pull-left">
										<i class="fa fa-caret-right"></i>
								</span> Popular City
								</a>
							</h4>
						</div>
						<div id="collapseCityNames" class="panel-collapse collapse in">
							<div class="panel-body ">
								<ul class="nav nav-pills nav-stacked tree" id="categoryFilter">
									<c:forEach items="${popularCityList}" var="pCList">
										<li class="dropdown-tree"><a href=""> <c:out
													value="${pCList.city_name}" /></a></li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>
					<div class="panel panel-default hidden">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="collapseWill active " data-toggle="collapse"
									href="#collapsePrice"> Price <span class="pull-left">
										<i class="fa fa-caret-right"></i>
								</span>
								</a> <span class="pull-right clearFilter  label-danger">
									Clear </span>
							</h4>
						</div>
						<div id="collapsePrice" class="panel-collapse collapse in">
							<div class="panel-body priceFilterBody">
								<div id="priceFilterBody"></div>

								<hr>
								<p>Enter a Price range</p>
								<form class="form-inline " role="form">
									<div class="form-group">
										<input type="text" class="form-control"
											id="exampleInputEmail2" placeholder="2000 $">
									</div>
									<div class="form-group sp">-</div>
									<div class="form-group">
										<input type="text" class="form-control"
											id="exampleInputPassword2" placeholder="3000 $">
									</div>
									<button type="submit" class="btn btn-default pull-right">check</button>
								</form>
							</div>
						</div>
					</div>


					<div class="subFilters"></div>

					<div class="panel panel-default hidden discountFilter">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" href="#collapseThree"
									class="collapseWill active "> Discount <span
									class="pull-left"> <i class="fa fa-caret-right"></i></span>
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse in">
							<div class="panel-body" id="discuontFilter"></div>
						</div>
					</div>

				</div>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-12">
				<div class="w100 clearfix categoryCls category-top">
					<h5 id="categoryHead">Restaurant's List</h5>
					<div class="categoryImage">
						<img id="categoryImg"
							src="https://192.168.0.135/ecommerce/images/default-1.jpg"
							class="img-responsive" alt="img" style="display: none;">
					</div>
				</div>



				<div class="w100 clearfix">
					<!-- ---------------------------------------------------------------------------- -->



					<div class="well well-sm">
						<div class="btn-group">
							<a href="#" id="list" class="btn btn-default btn-sm"><span
								class="glyphicon glyphicon-th-list"> </span>List</a> <a href="#"
								id="grid" class="btn btn-default btn-sm"><span
								class="glyphicon glyphicon-th"></span>Grid</a>
						</div>
					</div>
					<div class="row list-group">
						<c:forEach items="${addressListnrestList}" var="ALnRL">
							<div class="col-lg-3 col-md-6 item">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<div class="row">
											<div class="thumbnail img-container">
												<img class="cropped"
													src="../images/<c:out value="${ALnRL.restaurant.imageName}"/>"
													alt="Restaurant Images" height="200px" width="200px;"
													style="max-width: 200%; max-height: 100%" />
											</div>
										</div>
									</div>
									<a href="#">
										<div class="panel-footer">
											<a
												href="<c:url value="searchRestaurantDetails">
														<c:param name='restaurantId' value="${ALnRL.restaurant.id}"/>
													</c:url>"><h5
													class="group inner list-group-item-heading">
													<c:out value="${ALnRL.restaurant.name}" />
												</h5></a>

											<div class="row">
												<div class="col-xs-6 col-lg-6">
													<a
														href="<c:url value="/searchRestaurantDetails">
														<c:param name='restaurantId' value="${ALnRL.restaurant.id}"/>
													</c:url>">
														Details </a>
												</div>
											</div>

										</div>
									</a>
								</div>
							</div>


							<%-- <div class="item  col-xs-4 col-lg-4">
								<div class="thumbnail img-container">
									<div class="caption">
										<a href="#"><h5
												class="group inner list-group-item-heading">
												<c:out value="${ALnRL.restaurant.name}"></c:out>
											</h5></a>

										<div class="row">
											<div class="col-xs-6 col-lg-6">
												<a href=""> Details </a>
											</div>
										</div>
									</div>
								</div>

							</div> --%>
						</c:forEach>
						<!-- <div class="item  col-xs-4 col-lg-4">
							<div class="thumbnail">
								<img class="group list-group-image"
									src="http://placehold.it/400x250/000/fff" alt="" />
								<div class="caption">
									<h4 class="group inner list-group-item-heading">Product
										title</h4>
									<p class="group inner list-group-item-text">Product
										description... Lorem ipsum dolor sit amet, consectetuer
										adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
										laoreet dolore magna aliquam erat volutpat.</p>
									<div class="row">
										<div class="col-xs-12 col-md-6">
											<p class="lead">$21.000</p>
										</div>
										<div class="col-xs-12 col-md-6">
											<a class="btn btn-success"
												href="http://www.jquery2dotnet.com">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item  col-xs-4 col-lg-4">
							<div class="thumbnail">
								<img class="group list-group-image"
									src="http://placehold.it/400x250/000/fff" alt="" />
								<div class="caption">
									<h4 class="group inner list-group-item-heading">Product
										title</h4>
									<p class="group inner list-group-item-text">Product
										description... Lorem ipsum dolor sit amet, consectetuer
										adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
										laoreet dolore magna aliquam erat volutpat.</p>
									<div class="row">
										<div class="col-xs-12 col-md-6">
											<p class="lead">$21.000</p>
										</div>
										<div class="col-xs-12 col-md-6">
											<a class="btn btn-success"
												href="http://www.jquery2dotnet.com">Add to cart</a>
										</div>
									</div>
								</div>
							</div>
						</div>-->
					</div>


					<!-- ---------------------------------------------------------------------------- -->


				</div>
				<div class="row  categoryProduct xsResponse clearfix"></div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		$(document).ready(function() {
			$('#list').click(function(event) {
				event.preventDefault();
				$('#products .item').addClass('list-group-item');
			});
			$('#grid').click(function(event) {
				event.preventDefault();
				$('#products .item').removeClass('list-group-item');
				$('#products .item').addClass('grid-group-item');
			});
		});
	</script>


</body>
</html>